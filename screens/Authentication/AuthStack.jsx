import {createNativeStackNavigator} from '@react-navigation/native-stack';

import SplashScreen from './SplashScreen';
import SignIn from './SignIn';
import SighUp from './SighUp';
const Stack = createNativeStackNavigator();
const AuthStack = ({navigation, route}) => {
  return (
    <Stack.Navigator screenOptions={{headerMode: 'none', headerShown: false}}>
      <Stack.Screen name="SplashScreen" component={SplashScreen} />
      <Stack.Screen name="SignIn" component={SignIn} />
      <Stack.Screen name="SighUp" component={SighUp} />
    </Stack.Navigator>
  );
};

export default AuthStack;
