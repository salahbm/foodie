import React from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import {COLORS} from '../../constants/theme';
import {logo, map} from '../../assests';
const SplashScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image source={logo} style={styles.logo} resizeMode="contain" />
      </View>
      <View style={styles.footer}>
        <Text style={styles.title}>Explore New Yummy Places</Text>
        <Text style={styles.text}>Find the closest shops to you</Text>
        <View style={styles.button}>
          <TouchableOpacity
            style={styles.signIn}
            onPress={() => navigation.navigate('SignIn')}>
            <Text style={styles.textSign}>Get Started</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default SplashScreen;

const {height} = Dimensions.get('screen');
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.violent,
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
    backgroundColor: '#89B1DE',
  },
  logo: {
    width: 350,

    height: 350,
  },
  title: {
    color: '#05375a',
    fontSize: 35,
    fontWeight: 'bold',
  },
  text: {
    color: 'grey',
    marginTop: 5,
    color: 'white',
  },
  button: {
    alignItems: 'flex-end',
    marginTop: 30,
  },
  signIn: {
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
    backgroundColor: '#185CE3',
  },
  textSign: {
    color: 'white',
    fontWeight: 'bold',
  },
});
