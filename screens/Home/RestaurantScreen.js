import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Dimensions,
  Platform,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import {useRoute} from '@react-navigation/native';
import {Provider, Modal, Portal, Button} from 'react-native-paper';
import {menuData} from '../../constants/menudata';

const RestaurantScreen = ({navigation}) => {
  const route = useRoute();
  const restaurant = route.params?.restaurant;
  const [visible, setVisible] = useState(false);

  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const imgData = [
    {image: restaurant.photo1, id: 1},
    {image: restaurant.photo2, id: 2},
    {image: restaurant.photo3, id: 3},
  ];

  // Filter the menu items based on the restaurant name
  const restaurantMenu = menuData.filter(
    item => item.restId === restaurant.name,
  );
  console.log(restaurantMenu);
  return (
    <Provider>
      <View style={styles.container}>
        <View
          style={{alignItems: 'center', flexDirection: 'row', marginBottom: 5}}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{flex: 0.5}}>
            <Entypo name="chevron-left" size={30} color="#333" />
          </TouchableOpacity>
          <Text style={styles.textHeader}>{restaurant?.name}</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <ScrollView horizontal={true} pagingEnabled>
            {imgData.map(item => (
              <Image
                resizeMode="contain"
                key={item.id}
                source={{uri: item.image}}
                style={styles.courusel}
              />
            ))}
          </ScrollView>
          <RestaurantInfo restaurant={restaurant} />
          <View style={styles.businesshrs}>
            <Text style={styles.text}>Business Hours :</Text>

            <Button icon={'eye'} textColor="#FFFF" onPress={showModal}>
              Show
            </Button>
          </View>
          <BusinessHoursModal
            hideModal={hideModal}
            visible={visible}
            restaurant={restaurant}
          />

          {restaurantMenu.map(item => (
            <View key={item.pk} style={styles.menuItemContainer}>
              <Image
                source={item.photoUrl}
                style={styles.menuimage}
                resizeMode="contain"
              />
              <View style={styles.form}>
                <Text style={styles.placeholderText}>{item.name}</Text>
                <Text style={styles.desc}>{item.description}</Text>
                <Text style={styles.placeholderText}>{item.price}</Text>
              </View>
            </View>
          ))}
        </ScrollView>
      </View>
    </Provider>
  );
};

const RestaurantInfo = ({restaurant}) => {
  return (
    <View style={{width: width * 0.9}}>
      <View style={styles.row}>
        <Text style={styles.text}>Address:</Text>
        <Text
          style={[styles.text1, {width: 300}]}
          ellipsizeMode="tail"
          numberOfLines={1}>
          {restaurant?.address}
        </Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.text}>Phone:</Text>
        <Text style={styles.text1}>{restaurant?.phone}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.text}>Business Number:</Text>
        <Text style={styles.text1}>{restaurant?.businessNum}</Text>
      </View>
      <View
        style={{
          borderWidth: 1,
          borderBottomColor: '#918f87',
          marginVertical: 10,
        }}
      />
      <Text style={styles.ownerNote}>
        {restaurant?.note ? restaurant?.note : 'No Notes Yet'}
      </Text>
      <View
        style={{
          borderWidth: 1,
          borderBottomColor: '#918f87',
          marginVertical: 10,
        }}
      />
    </View>
  );
};

const HoursRow = ({day, openTime, closeTime}) => {
  return (
    <View style={styles.rowHours}>
      <Text style={styles.text}>{day}</Text>
      <View style={styles.row}>
        <Text style={[styles.text, {color: '#4cd936'}]}>{openTime}</Text>
        <Text style={[styles.text, {color: '#fa7366', flex: 0.5}]}>
          {closeTime}
        </Text>
      </View>
    </View>
  );
};

const BusinessHoursModal = ({restaurant, visible, hideModal}) => {
  const containerStyle = {
    marginHorizontal: 20,
    padding: 20,
    backgroundColor: '#FFFF',
    height: height / 3,
    borderRadius: 10,
  };
  return (
    <Portal>
      <Modal
        visible={visible}
        onDismiss={hideModal}
        contentContainerStyle={containerStyle}>
        <HoursRow day="Day" openTime={'Open'} closeTime={'Finish'} />
        <HoursRow
          day="Monday"
          openTime={restaurant.Monday_start}
          closeTime={restaurant.Monday_end}
        />

        <HoursRow
          day="Tuesday"
          openTime={restaurant.Tuesday_start}
          closeTime={restaurant.Tuesday_end}
        />
        <HoursRow
          day="Wednesday"
          openTime={restaurant.Wednesday_start}
          closeTime={restaurant.Wednesday_end}
        />
        <HoursRow
          day="Thursday"
          openTime={restaurant.Thursday_start}
          closeTime={restaurant.Thursday_end}
        />
        <HoursRow
          day="Friday"
          openTime={restaurant.Friday_start}
          closeTime={restaurant.Friday_end}
        />
        <HoursRow
          day="Saturday"
          openTime={restaurant.Saturday_start}
          closeTime={restaurant.Saturday_end}
        />
        <HoursRow
          day="Sunday"
          openTime={restaurant.Sunday_start}
          closeTime={restaurant.Sunday_end}
        />
      </Modal>
    </Portal>
  );
};
export default RestaurantScreen;
const {height, width} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ebf0f2',
    marginTop: Platform.OS === 'ios' ? (height >= 700 ? 45 : 20) : 0,
    paddingHorizontal: 20,
  },
  textHeader: {
    fontSize: 30,
    color: '#055DF8',
    fontWeight: 700,
    alignSelf: 'center',
  },

  courusel: {
    height: 200,
    width: width * 0.9,
    borderRadius: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 5,
  },
  rowHours: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    justifyContent: 'space-between',
  },
  text: {
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'right',
  },
  text1: {
    fontSize: 15,
    fontWeight: '500',
    textAlign: 'right',
    width: 200,
  },
  ownerNote: {
    fontWeight: '400',
    fontSize: 15,
    textAlign: 'center',
  },
  businesshrs: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#669ffa',
    borderRadius: 10,
    marginTop: 20,
  },
  // menu styles
  menuItemContainer: {
    flexDirection: 'row',
    marginBottom: 20,
    alignItems: 'center',
  },
  menuimage: {
    width: 100,
    height: 100,
    resizeMode: 'cover',
    borderRadius: 12,
  },
  placeholder: {
    backgroundColor: '#ccc',
    alignItems: 'center',
    borderColor: '#055DF8',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 8,
  },
  placeholderText: {
    fontSize: 16,
    color: '#555',
  },
  desc: {
    fontSize: 12,
    width: 300,
    color: '#555',
  },
});
