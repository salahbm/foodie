import React, {useState, useEffect} from 'react';
import {
  View,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import * as ImagePicker from 'react-native-image-picker';

import Entypo from 'react-native-vector-icons/Entypo';

const AddMenuItemScreen = ({navigation}) => {
  const [menuItems, setMenuItems] = useState([]);
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [description, setDescription] = useState('');
  const [image, setImage] = useState(null);

  // Function to handle the form submission
  const handleSubmit = () => {
    // Perform validation and add the menu item to the array
    const menuItemData = {
      name,
      price,
      description,
      image,
    };

    setMenuItems([...menuItems, menuItemData]);

    // Reset the form fields
    setName('');
    setPrice('');
    setDescription('');
    setImage(null);
  };
  const handleDeleteLastForm = () => {
    if (menuItems.length > 0) {
      const updatedMenuItems = [...menuItems];
      updatedMenuItems.pop();
      setMenuItems(updatedMenuItems);
    }
  };

  const handleImageSelection = () => {
    const options = {
      title: 'Select Image',
      mediaType: 'photo',
      quality: 0.7,
      nodata: true,
    };

    ImagePicker.launchImageLibrary(options, response => {
      if (response.uri) {
        console.log('Image selection worked');
        setImage(response.uri);
      } else if (response.error) {
        console.error('ImagePicker Error:', response.error);
      } else if (response.didCancel) {
        console.log('image canceled');
      }
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{alignItems: 'center', flexDirection: 'row'}}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Settings')}
          style={{flex: 0.5}}>
          <Entypo name="chevron-left" size={30} color="#333" />
        </TouchableOpacity>
        <Text style={styles.textHeader}>Add Menu Items:</Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        {menuItems.map((menuItem, index) => (
          <View key={index} style={styles.menuItemContainer}>
            <TouchableOpacity onPress={handleImageSelection}>
              {image ? (
                <Image source={{uri: image}} style={styles.image} />
              ) : (
                <View style={styles.placeholder}>
                  <Text style={styles.placeholderText}>Select Image</Text>
                </View>
              )}
            </TouchableOpacity>

            <View style={styles.form}>
              <TextInput
                style={styles.input}
                placeholder="Menu Item Name"
                value={menuItem.name}
                onChangeText={text => {
                  const updatedMenuItems = [...menuItems];
                  updatedMenuItems[index].name = text;
                  setMenuItems(updatedMenuItems);
                }}
              />
              <TextInput
                style={styles.input}
                placeholder="Price"
                value={menuItem.price}
                onChangeText={text => {
                  const updatedMenuItems = [...menuItems];
                  updatedMenuItems[index].price = text;
                  setMenuItems(updatedMenuItems);
                }}
              />
              <TextInput
                style={styles.input}
                placeholder="Description"
                value={menuItem.description}
                onChangeText={text => {
                  const updatedMenuItems = [...menuItems];
                  updatedMenuItems[index].description = text;
                  setMenuItems(updatedMenuItems);
                }}
                multiline
              />
            </View>
          </View>
        ))}
        <View
          style={{
            alignItems: 'center',
            flexDirection: 'row',
            alignSelf: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: 24,
          }}>
          <TouchableOpacity onPress={handleSubmit} style={{flex: 1}}>
            <Text style={styles.addButton}>+</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleDeleteLastForm} style={{flex: 1}}>
            <Text style={styles.delButton}>-</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
  },
  menuItemContainer: {
    flexDirection: 'row',
    marginBottom: 20,
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 100,
    resizeMode: 'cover',
    borderRadius: 12,
  },
  placeholder: {
    width: 110,
    height: 110,
    backgroundColor: '#ccc',
    alignItems: 'center',
    borderColor: '#055DF8',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 8,
  },
  placeholderText: {
    fontSize: 16,
    color: '#555',
  },
  form: {
    flex: 1,
    marginLeft: 10,
  },
  input: {
    borderColor: '#055DF8',
    borderWidth: 1,
    borderRadius: 8,
    height: 30,
    marginVertical: 5,
    paddingHorizontal: 10,
  },
  addButton: {
    fontSize: 30,
    color: 'blue',
    alignSelf: 'center',
  },
  delButton: {
    fontSize: 40,
    color: 'blue',
    alignSelf: 'center',
  },
  textHeader: {
    textAlign: 'center',

    marginVertical: 10,
    fontSize: 25,
    fontWeight: 'bold',
  },
});

export default AddMenuItemScreen;
