import axios from 'axios';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';
import {Button} from 'react-native-paper';
import Entypo from 'react-native-vector-icons/Entypo';

const TermsConditiinos = ({navigation}) => {
  const [restaurants, setRestaurants] = useState({
    name: '',
    businessNum: '',
  });

  return (
    <View style={styles.container}>
      <View style={{alignItems: 'center', flexDirection: 'row'}}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{flex: 0.5}}>
          <Entypo name="chevron-left" size={30} color="#333" />
        </TouchableOpacity>

        <Text style={styles.textHeader}>Terms and Conditions</Text>
      </View>
    </View>
  );
};
const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ebf0f2',
    marginTop: Platform.OS === 'ios' ? (height >= 700 ? 35 : 10) : 0,
    paddingHorizontal: 20,
  },
  text: {
    textAlign: 'left',
    marginVertical: 20,
    fontSize: 15,
    fontWeight: '600',
    marginHorizontal: 5,
  },
  textHeader: {
    textAlign: 'center',

    marginVertical: 10,
    fontSize: 25,
    fontWeight: 'bold',
  },
  input: {
    width: width * 0.9,
    alignSelf: 'center',
    padding: 10,
    height: 40,
    backgroundColor: '#ebb2a9',
    borderRadius: 10,
  },
  note: {
    textAlign: 'left',
    marginVertical: 20,
    marginLeft: 10,
    fontSize: 12,
    fontWeight: '500',
  },
});

export default TermsConditiinos;
