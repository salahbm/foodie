const stew = require('../Menu/kimchijjigae.png');
const sushi = require('../Menu/sushi_combo.png');
const tonkatsu = require('../Menu/galbi.png');
const bibimbap = require('../Menu/bibimpap.png');
const teriyakiDonburi = require('../Menu/teriyaki_donburi.png');
const misoSoup = require('../Menu/miso_soup.png');
const udon = require('../Menu/udon.png');
const seafoodPancake = require('../Menu/seafood_pancake.png');
const garlicBread = require('../Menu/garlic_bread.png');
const margarita = require('../Menu/margarita.png');
const pepperoni = require('../Menu/peperoni.png');
const vegetarianPizza = require('../Menu/vegetarian_pizza.png');
const fruitYogurt = require('../Menu/fruityogurt.png');
const brownie = require('../Menu/brownie.png');
const croissant = require('../Menu/croissant.png');
const americano = require('../Menu/americano.png');
const japchae = require('../Menu/japchae.png');
const kimchiFriedRice = require('../Menu/kimchi_fried_rice.png');

export {
  stew,
  sushi,
  tonkatsu,
  bibimbap,
  teriyakiDonburi,
  misoSoup,
  udon,
  seafoodPancake,
  garlicBread,
  margarita,
  pepperoni,
  vegetarianPizza,
  fruitYogurt,
  brownie,
  croissant,
  americano,
  japchae,
  kimchiFriedRice,
};
