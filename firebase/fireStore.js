import firebase from 'firebase/app';
import 'firebase/storage';



// Initialize Firebase app
const firebaseConfig = {
    apiKey: "AIzaSyCrGRxYloy8EhGvABZNpcIkGlxDp9-dgoM",
    authDomain: "foodie-8016e.firebaseapp.com",
    projectId: "foodie-8016e",
    storageBucket: "foodie-8016e.appspot.com",
    messagingSenderId: "941443982288",
    appId: "1:941443982288:web:9a585560cb6e2c9f78cfd5",
    measurementId: "G-EW0QWD39XJ"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

// Function to upload photo to Firebase Storage
const uploadPhotoToFirebase = async (photoUri) => {
  const storageRef = firebase.storage().ref();
  const fileName = 'restaurantImg'; // Set a unique file name here
  const photoRef = storageRef.child(fileName);

  // Convert the photo URI to a Blob object
  const response = await fetch(photoUri);
  const blob = await response.blob();

  // Upload the photo to Firebase Storage
  await photoRef.put(blob);

  // Get the download URL of the uploaded photo
  const downloadUrl = await photoRef.getDownloadURL();

  // Save the download URL to MySQL
  savePhotoUrlToMySQL(downloadUrl);
};
