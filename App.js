import React, {useEffect, useState, useCallback} from 'react';
import {Provider} from 'react-redux';
import Tabs from './navigation/tabs';
import {NavigationContainer} from '@react-navigation/native';
import {store} from './store/store';
import {Provider as PaperProvider} from 'react-native-paper';
import {ContexData} from './constants/useContext';
import axios from 'axios';
import {apiURL} from './constants/apiURL';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loading from './components/Loading';
import AuthStack from './screens/Authentication/AuthStack';
const App = () => {
  const [data, setData] = useState([]);
  const [menu, setMenu] = useState([]);
  const [loading, setLoading] = useState(false);
  const [loggedInUser, setLoggedInUser] = useState(null);

  useEffect(() => {
    setLoading(true);
    function getDatas() {
      axios
        .get(apiURL)
        .then(response => setData(response.data))
        .catch(error => {
          console.error('Error fetching data from server:', error);
        });
      setLoading(false);
    }
    async function getMenu() {
      try {
        const response = await axios.get(apiURL);
        setMenu(response.data);
      } catch (error) {
        console.error(error);
      }
    }
    getDatas();
    getMenu();
  }, []);
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 1100);
    //LOGIN LOGICS
    // Check if there's a logged-in user
    const checkLoggedInUser = async () => {
      try {
        const user = await AsyncStorage.getItem('loggedInUser');
        console.log(user);
        if (user) {
          setLoggedInUser(JSON.parse(user));
        }
      } catch (error) {
        console.log(error);
      }
    };
    checkLoggedInUser();
  }, []);

  //log out logic

  const handleLogout = useCallback(async () => {
    await AsyncStorage.removeItem('loggedInUser');
    //to delete all the data in storage
    // await AsyncStorage.removeItem('users');

    setLoggedInUser(null);
  }, [loggedInUser]);

  return (
    <NavigationContainer>
      <ContexData.Provider
        value={{data, loading, menu, handleLogout, setLoggedInUser}}>
        <Provider store={store}>
          <PaperProvider>
            {loading ? <Loading /> : loggedInUser ? <Tabs /> : <AuthStack />}
          </PaperProvider>
        </Provider>
      </ContexData.Provider>
    </NavigationContainer>
  );
};

export default App;
